<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string("title");
            $table->integer("quantity")->default(1);
            $table->text("image_urls")->nullable(); //json
            $table->enum("type",["single","countable","sale"])->nullable();
            $table->enum("status",["broken","good","repair","draft"])->default("good");
            $table->float("purchase_price")->nullable();
            $table->float("sale_price")->nullable();
            $table->float("deposit")->nullable();
            $table->float("daily_rent")->nullable();
            $table->unsignedBigInteger("owner_user_id")->nullable();
            $table->timestamps();

            $table->string("qrcode")->nullable();

            $table->foreign("owner_user_id")->references("id")->on("users")->onDelete("cascade");

        });
       
        Schema::create('product_rental', function (Blueprint $table) {

            $table->string("qrcode")->nullable();

            $table->unsignedBigInteger("rental_id");
            $table->unsignedBigInteger("product_id");
            $table->integer("quantity")->default(1);
            $table->enum('status', ['draft','reserved','booked','rented','delivered','returned','issue','archive'])->default("reserved");
            $table->unsignedBigInteger("employee_user_id")->nullable();
            $table->text("notes")->nullable();
            $table->string("product_single_ids")->nullable();
            
            $table->timestamps();

            $table->foreign("rental_id")->references("id")->on("rentals")->onDelete("cascade");
            $table->foreign("product_id")->references("id")->on("products")->onDelete("cascade");
            $table->foreign("employee_user_id")->references("id")->on("users")->onDelete("set null");
        });

        Schema::create('product_singles', function (Blueprint $table) {
            $table->id();
            $table->string("sn");
            $table->string("qrcode")->nullable();
            $table->unsignedBigInteger("product_id");
            $table->foreign("product_id")->references("id")->on("products")->onDelete("cascade");
        });

        Schema::create('product_single_notes', function (Blueprint $table) {
            $table->id();
            $table->string("notes")->nullable();
            $table->string("image_urls")->nullable(); //json
            $table->boolean("is_critical")->default(false);
            $table->unsignedBigInteger("employee_user_id")->nullable();
            $table->unsignedBigInteger("product_single_id");
            $table->foreign("product_single_id")->references("id")->on("product_singles")->onDelete("cascade");
            $table->foreign("employee_user_id")->references("id")->on("users")->onDelete("set null");
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_single_notes');
        Schema::dropIfExists('product_singles');
        Schema::dropIfExists('rental_has_product');
        Schema::dropIfExists('products');
    }
}
