<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->string("firstname");
            $table->string("lastname");
            $table->string("street_and_nr")->nullable();
            $table->string("city_and_zip")->nullable();
            $table->string("country")->nullable();
            $table->string("email")->nullable();
            $table->string("phone")->nullable();
            $table->string("logo_url")->nullable();
            $table->string("payment_info")->nullable(); // json
            $table->timestamps();

            $table->unsignedBigInteger("created_by_user_id");
            $table->foreign("created_by_user_id")->references("id")->on("users")->onDelete("cascade");


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
