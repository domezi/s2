<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rentals', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger("employee_user_id")->nullable();
            $table->unsignedBigInteger("customer_user_id");
            $table->unsignedBigInteger("project_id")->nullable();
            $table->unsignedBigInteger("address_id")->nullable();

            $table->integer("vat")->default(0); // 14%
            $table->integer("discount")->default(0); // 50%

            $table->date('start', 0)->nullable();
            $table->date('start_scheduled', 0)->nullable();

            $table->date('end', 0)->nullable();
            $table->date('end_scheduled', 0)->nullable();

            $table->enum('status', ['draft','reserved','booked','rented','delivered','returned','issue','archive'])->default("draft");

            $table->timestamps();

            $table->boolean("invoicing_enabled")->default(true);

            $table->foreign("employee_user_id")->references("id")->on("users")->onDelete("set null");
            $table->foreign("customer_user_id")->references("id")->on("users")->onDelete("cascade");
            $table->foreign("project_id")->references("id")->on("projects")->onDelete("cascade");
            $table->foreign("address_id")->references("id")->on("addresses")->onDelete("set null");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rentals');
    }
}
