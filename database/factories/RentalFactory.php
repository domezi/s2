<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Rental;
use Faker\Generator as Faker;

$factory->define(Rental::class, function (Faker $faker) {
    return [
        "customer_user_id"=>factory(\App\User::class)->create()->id,
        "project_id"=>factory(\App\Project::class)->create()->id
    ];
});
