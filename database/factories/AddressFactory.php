<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Address;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {
    return [
        "firstname"=>$faker->firstName,
        "lastname"=>$faker->lastName,
        "created_by_user_id"=>factory(\App\User::class)->create()
    ];
});
