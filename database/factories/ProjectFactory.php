<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Project;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    return [
        "title"=>$faker->sentence,
        "customer_user_id"=>factory(\App\User::class)->create()->id
    ];
});
