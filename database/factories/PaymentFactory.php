<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Payment;
use Faker\Generator as Faker;

$factory->define(Payment::class, function (Faker $faker) {
    return [
        "employee_user_id"=>factory(\App\User::class)->create()->id,
        "rental_id"=>factory(\App\Rental::class)->create()->id,
        "sum"=>rand(-2000*10,2000*10)/10
    ];
});
