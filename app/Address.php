<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $guarded = [];
    //
    public function displaySearchresult() {
        echo "<b>".$this->firstname." ".$this->lastname."</b>, ".$this->street_and_nr.", ".$this->city_and_zip;
    }
}
