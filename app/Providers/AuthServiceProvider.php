<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin', function ($user) {
            return $user->can_admin;
        });

        Gate::define('rent', function ($user) {
            return $user->can_rent;
        });

        Gate::define('rent_a_draft', function ($user, $rental) {
            return $rental->customer_user_id == $user->id  &&  $rental->status == "draft";
        });

    }
}
