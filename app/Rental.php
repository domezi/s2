<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rental extends Model implements Searchresult
{
    protected $guarded = [];

    public function days() {
        // how many days is the rental
        return( 
            (strtotime($this->end()) - strtotime($this->start())) / 86400 
        );  
    }
    public function color() {
        $color="secondary";
        if($this->is_overdue()) $color="danger";
        else if($this->status=="draft") $color="primary";
        else if($this->status=="rented") $color="success";
        else if($this->status=="returned") $color="secondary";
        else if($this->status=="archive") $color="light";
        else if($this->status=="issue") $color="warning";

        return $color;
    }


    public function title($colored = false) {

    $out='<span class="badge badge-pill badge-'.$this->color().'">'.__(ucfirst($this->status)).'</span>';
               $out .= " ".$this->getStartEndHtml();
        $out .= "<br><span style=color:#555;font-weight:100>".$this->customer->name.", ".$this->updated_at->diffForHumans()."</span>";

        return $out;
    }
    public function project_title() {
        return $this->project_id == null ? "-" : $this->project->title;
    }
    public function is_overdue() {

        return strtotime($this->end) < time() && $this->status == "rented";
    }
    public function getStartEndHtml() {
        return date("d. M Y", strtotime($this->start)).", ".$this->days()." Tage";
    }

    public function status() {
        return __(ucfirst($this->status));
    }

    public function displaySearchresult() {
        echo "<div class='py-2 mx-0 px-0 row hoverline border-top'>
            <div class=col>
                <a class='text-dark' href='".route("rentals.show",$this->id)."'>".$this->title(true)."</a>
            </div>
            <div class=col>
            <span>".$this->currency($this->total_sum())."</span>
            </div>
            <div class=col>
            <span>".($this->total_quantity())." ".__("Artikel")."</span>,
            <span>".count($this->products)." ".__("verschiedene")."</span><br>

            <form method='post' style='' action='".route('rentals.current_open',$this->id)."'>
                <input name='_token' type='hidden' value='".csrf_token()."'>
                <button class='btn btn-outline-success btn-sm' >".__('Als aktuelle Leihliste öffnen')." »</button>
            </form>


            </div>
        </div>";
        } 


    public function start() {
        return ($this->start == null) ? $this->start_scheduled : $this->start;
    }

    public function end() {
        return ($this->end == null) ? $this->end_scheduled : $this->end;
    }

    public function products() {
        return $this->belongsToMany('App\Product')
                    ->withPivot(
                        'quantity',
                        'product_single_ids',
                        'notes',
                        'status'
                    );
    }

    public function payments() {
        return $this->hasMany('App\Payment');
    }

    public function payments_sum() {
        $sum=0;
        foreach($this->payments as $payment) {
            $sum+=$payment->sum;
        }
        return $sum;
    }

    public function total_sum() {
        return (
            $this->daily_rent()*$this->days()+ 
            $this->sale_price()+
            $this->deposit()*0
        )*(100+$this->vat-$this->discount)/100-$this->payments_sum();
    }

    public function daily_rent() {
        $sum=0;
        foreach($this->products as $product) {
            $sum+=$product->daily_rent*$product->pivot->quantity;
        }
        return $sum;
    }
    
    public function currency($euros) { return number_format($euros, 2, ',', '.')."€"; }



    public function sale_price() {
        $sum=0;
        foreach($this->products as $product) {
            $sum+=$product->sale_price*$product->pivot->quantity;
        }
        return $sum;
    }

    public function total_quantity() {
        $sum=0;
        foreach($this->products as $product) {
            $sum += $product->pivot->quantity;
        }
        return $sum;
    }

    public function deposit() {
        $sum=0;
        foreach($this->products as $product) {
            $sum+=$product->deposit*$product->pivot->quantity;
        }
        return $sum;
        /*
        return array_reduce($this->products->toArray(), function($product) {
            return $product["deposit"];
            //return $product->deposit;
        }, 0);
         */
    
    }

    public function project() {
        return $this->belongsTo('App\Project',"project_id");
    }
    public function employee() {
        return $this->belongsTo('App\User',"employee_user_id");
    }
    public function customer() {
        return $this->belongsTo('App\User',"customer_user_id");
    }
    public function isOwner(\App\User $user = null) {
        return $user->id == $this->customer->id;
    }
    public function set_status(\App\Product $product, $status = "rented") {
        $old_relation = $this->products()->find($product->id);
        $sql="update product_rental set status = '".$status."' where rental_id = '".$this->id."' and product_id = '".$product->id."'";
        return \DB::statement($sql);
    }

    public function rent(\App\Product $product, $quantity = 1, $product_single_ids = []) {

        $old_relation = $this->products()->find($product->id);
        if($old_relation != null) {

            // remove if zero quantity
            if($old_relation->pivot->quantity + $quantity == 0) {
                $sql="delete from product_rental where rental_id = '".$this->id."' and product_id = '".$product->id."'";
                \DB::statement($sql);
                return;
            }


            // only upadte quantity and sns
            //"update product_rental set quantity = quantity + ".intval($quantity).", product_single_ids = '".  json_encode( array_merge($product_single_ids, json_decode($old_relation->pivot->product_single_ids))) ."'
            //where rental_id = '".$this->id."' and product_id = '".$product->id."' limit 1"
            $sql="update product_rental set quantity = quantity + ".intval($quantity).", product_single_ids = '".  json_encode( array_merge($product_single_ids, json_decode($old_relation->pivot->product_single_ids))) ."' where rental_id = '".$this->id."' and product_id = '".$product->id."'";
            \DB::statement($sql);



            //die($sql);
            /*
            $this->products()->syncWithoutDetaching(
                [
                    $product->id  =>  [ 
                        "quantity"          => $old_relation->pivot->quantity + $quantity,
                        "product_single_ids"=> json_encode(
                            array_merge($product_single_ids, json_decode($old_relation->pivot->product_single_ids))
                        )
                    ]
                ]
            );
             */

        } else {
            $this->products()->attach($product,["quantity"=>$quantity,"product_single_ids"=>json_encode($product_single_ids)]);
        } 
    }

    public function remove(\App\Product $product, $quantity = 1, $serial_numbers = []) {
        //implement
        //$this->products->remove($product)->save();
    }

    public function isSelfContainedRental() {
        return $this->customer == $this->employee;
    }

}
