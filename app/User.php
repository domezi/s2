<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use \App\Project;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [ ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function address() {
        $a= \App\Address::find($this->default_address_id);
        $a_fallback= \App\Address::find(1);
        return ($a == null) ? $a_fallback : $a;
    }

    public function displaySearchresult() {
        $a= \App\Address::find($this->default_address_id);
        $a_fallback="<b>".$this->name."</b>";
        return ($a == null) ? $a_fallback : $a->displaySearchresult();
    }

    public function setDefaultAddress(\App\Address $address) {
        $this->default_address_id=$address->id;
        $this->save();
    }
    public function name() {
        return $this->name;
    }

    public function projects() {
        return $this->hasMany('App\Project',"customer_user_id");
    }

    public function addresses() {
        return $this->hasMany('App\Address',"created_by_user_id");
    }

    public function createProject($title="") {
        if($title == "")
            $title=__("Unnamed project");
        $this->projects()->save(
            new Project(compact("title"))
        );

    }


}
