<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public function rental() {
        return $this->belongsTo('App\Rental');
    }

    public function project() {
        return $this->rental->project();
    }
    public function employee() {
        return $this->belongsTo('App\User',"employee_user_id");
    }

}
