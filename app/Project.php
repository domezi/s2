<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model implements Searchresult 

{
    protected $guarded = [];
    public function owner() {
        return $this->belongsTo('App\User',"customer_user_id");
    }
    public function isOwner(\App\User $user = null) {
        return $user->id == $this->owner->id;
    }
    public function rentals() {
        return $this->hasMany('App\Rental');
    }

    public function displaySearchresult() {
        echo "<h1>".$this->title."</h1>";
        if($this->location) {
            echo "<span class='mb-2 ml-1'><i class='fa fa-map-marker mx-2'></i> ".$this->location."</span>";
    }
        echo "<div>";
        foreach($this->rentals as $rental) {
            if($rental->status !== "archive")
                echo $rental->displaySearchresult();
        }
        echo "<div class='border-top'>";
               echo "<form method=post class='d-inline-block px-2 py-2 ' action='".route("rentals.store")."'>
                <input name='_token' type='hidden' value='".csrf_token()."'>
            <input type=hidden name=project_id value='".$this->id."'>
            <input type=hidden name=customer_user_id value='".$this->owner->id."'>
            <input type=submit class='btn btn-link btn-sm ' value='".__("+ Leihliste erstellen")."'>
            </form>";
        echo "<form method=get class='d-inline-block py-2' action='".route("projects.edit",$this)."'>
                <input type=submit class='btn btn-link btn-sm ' value='".__("Projekt bearbeiten")."'>
            </form>";


        echo "</div>";
        echo "</div>";
    } 


    public function total_sum() {
        $sum=0;
        foreach($this->rentals as $rental) {
            $sum+=$rental->total_sum();
        }
        return $sum;
    }


    public function daily_rent() {
        $sum=0;
        foreach($this->rentals as $rental) {
            $sum+=$rental->daily_rent();
        }
        return $sum;
    }


    public function sale_price() {
        $sum=0;
        foreach($this->rentals as $rental) {
            $sum+=$rental->sale_price();
        }
        return $sum;
    }

    public function deposit() {
        $sum=0;
        foreach($this->rentals as $rental) {
            $sum+=$rental->deposit();
        }
        return $sum;
        /*
        return array_reduce($this->rentals->toArray(), function($rental) {
            return $rental["deposit"];
            //return $rental->deposit;
        }, 0);
         */
    
    }

    /*
    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);

        $this->employee_user_id = \Auth::id();
    }
     */
}
