<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model implements Searchresult 
{

    protected $guarded = [];
    public function isProductSingle() { return $this->type == "single"; }
    public function isProductCountable() { return $this->type == "countable"; }
    public function isProductSale() { return $this->type == "sale"; }

    public function displaySearchresult() {
        echo "<div class='search-result'>
            <a href='".route("products.show",$this->id)."' class='text-dark'>
            <img src='".$this->thumb()."' class=thumb>
            <div>
            <h1>".$this->title."</h1>
            <p>".$this->getPricingHtml()."</p></a>";

                        echo '<form method="post" class="d-flex" action="'.route("products.rent", $this->id).'">
                            <input name="_token" value="'.csrf_token().'" type=hidden>
                            <input name="quantity" value="1" class="form-control" type="number" style="width:80px"> 
                            <button class="btn btn-primary">'.("Auf aktuelle Liste").'</button>
                        </form>';
            echo "
            </div>
        </div>";
    } 
    public function thumb() {
$thumb = explode(",",$this->image_urls)[0];
return $thumb==null ? "https://ppc.tools/wp-content/themes/ppctools/img/no-thumbnail.jpg" : $thumb;
    }

    public function getQuantityHtml() {
        return $this->quantity;
    }
    public function getPricingHtml() {
        return ($this->daily_rent> 0) ? $this->daily_rent()."<span style=font-size:.7em;margin:3px>/Tag</span>":$this->sale_price();
    }

    private function currency($euros) { return number_format($euros, 2, ',', '.')."&euro;"; }

    public function daily_rent() { return $this->currency($this->daily_rent);}
    public function sale_price() { return $this->currency($this->sale_price);}
    public function deposit() { return $this->currency($this->deposit);}

    public function getQuantityAvailableCurrent() {
        $rental = \App\Rental::find(session("current_rental_id"));
        if($rental == null)  
            return $this->getQuantityAvailable(date("Y-m-d"), date("Y-m-d",strtotime("+1 days")));
        else
            return $this->getQuantityAvailable($rental->start,$rental->end);
    }
    public function getQuantityAvailable($start, $end) {
        // check start and end for greatiness
        // alle ausgebuchten finden
        if($start == null || $end == null) return $this->quantity ;
        $quantity_minus = DB::select( DB::raw("SELECT sum(pr.quantity) as sum FROM product_rental pr, rentals r WHERE 
            pr.rental_id = r.id and
            pr.product_id = '".$this->id."' and 
            (
                ('".$start."' < r.end and '".$end."' > r.end)
                or ('".$end."' > r.start and '".$start."' < r.start)
                or 
                (
                    '".$start."' > r.start and 
                    '".$end."' < r.end
                )
            )
          ") );
        return (   $this->quantity - $quantity_minus[0]->sum  );
    }
     
    public function rentals() {
        $rs = DB::select( DB::raw("SELECT rental_id FROM product_rental WHERE product_id = '".$this->id."'"));
        $rentals = [];
        foreach($rs as $r) {
            $rentals[]=\App\Rental::find($r->rental_id);
        }
        return $rentals;
    }

    public function image_urls() {
        return json_decode($this->image_urls);
    }

    public function title($maxlength=100) {
        return substr($this->title, 0, $maxlength);
    }

    public function sum($days) {
        return $this->quantity * $this->sale_price + $this->quantity * $this->daily_rental * $days; 
    }
    public function getOwnerHtml() {
        if($this->owner != null) return __("von")." <a href='#'>".$this->owner->name()."</a>";
    }

    public function location() {
        return $this->belongsTo('App\Location');
    }

    public function singlesDepricated() {
        return \App\ProductSingle::where("product_id","=",$this->id)->get();
    }

    public function singles() {
        return $this->belongsToMany('App\ProductSingle',"product_singles","product_id");
    }

    public function owner() {
        return $this->belongsTo('App\User',"owner_user_id");
    }
    public function isOwner(\App\User $user = null) {
        return $user->id == $this->owner->id;
    }

}
