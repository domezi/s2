<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Gate;

class UserController extends Controller
{
    public function index(Request $request) {
        if(\Gate::allows("admin"))
            return view("pages.index_users",["users"=>\App\User::latest()->paginate(15)]);
    }

    public function store(Request $request) {
        $data=$request->validate(["email"=>"unique:users,email|required|email","lname"=>"required","fname"=>"required"]);
        User::create([
            "email"=>$data["email"],
            "name"=>$data["fname"]." ".$data["lname"],
            "password"=>"§ER%&T/Z/U()I=(U/Z&T%ER&T/Z(U))"
        ]);
        return back();

    }
    public function create(Request $request) {
        return view("pages.create_user");
    }
  public function update(Request $request, User $user) {
      if(request("create_address")!="") {
        $a=explode(",",request("create_address"));
        $u=explode(" ",$user->name);
        $a = \App\Address::create([
            "firstname"=>$u[0],
            "lastname"=>$u[1],
            "street_and_nr" => $a[0],
            "city_and_zip" => $a[1],
            "created_by_user_id" => $user->id
        ]);
        $val = ["default_address_id"=>$a->id];
      } else {

          if (Gate::allows('admin')) { 
              $rules=[
                  'can_admin' => "",
                  'can_rent' => "",
              ];
              $val=request()->validate($rules);
          } else {
              //return back();
          }
      }
 
        $user->update($val);
        return back();
    }


}
