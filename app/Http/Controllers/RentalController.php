<?php

namespace App\Http\Controllers;

use App\Rental;
use Gate;
use Illuminate\Http\Request;

class RentalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Gate::allows("rent"))
            $rentals = Rental::paginate(15);
        else
            $rentals = Rental::where('customer_user_id', '=', \Auth::id())->paginate(15);
        return view("pages.index_rentals",compact("rentals"));


    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function current_close()
    {
            request()->session()->put('current_rental_id', null);
            return back();
    }

   
    public function request_reservation(Rental $rental)
    {
        if(mail("info@ziegenhagel.com","S2: Reservierung beantragt","Diese Liste soll reserviert werden: https://s2.covomedia.com/rentals/".$rental->id."/"))
            return "<div style='text-align:center;margin:150px auto'>
                <h1>Bestätigung</h1><p>Deine Reservierung wird beantragt, Du bekommst in Kürze Bescheid!</p><a href='/'>&larr; Zurück</a></div>";
        else
            return "<div style='text-align:center;margin:150px auto'>
                <h1>Ein Fehler ist aufgetreten</h1>
                <p>Bitte sende folgende Adresse an Dominik: https://s2.covomedia.com/rentals/".$rental->id."/</p><a href='/'>&larr; Zurück</a></div>";
    }
    public function current_open(Rental $rental)
    {
        request()->session()->put('current_rental_id', $rental->id);
        return back();
            //return redirect(route("rentals.index"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->validate([
            "project_id"=>"exists:projects,id|nullable",
            "customer_user_id"=>"exists:users,id|required",
        ]);

        if(!\Gate::allows("rent") && !\Gate::allows("admin"))
            $data["customer_user_id"]=\Auth::id();

        $data["employee_user_id"]=\Auth::id();

        $rental= Rental::create($data);

        return $this->current_open($rental);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rental  $rental
     * @return \Illuminate\Http\Response
     */
    public function show(Rental $rental)
    {
        return view("pages/show_rental",compact("rental"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rental  $rental
     * @return \Illuminate\Http\Response
     */
    public function edit(Rental $rental)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rental  $rental
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rental $rental)
    {
        if (Gate::allows('rent')) { 
            $rules=[
                'customer_user_id' => "numeric",
                'notes' => "nullable",
                'employee_user_id' => "numeric",
                'project_id' => "exists:projects,id|nullable",
                'status' => "in:draft,reserved,booked,rented,delivered,returned,issue,archive",
                'start' => "date|nullable",
                'end' => "date|nullable",
            ];
        } else if(Gate::allows("rent_a_draft",$rental)) {
                $rules=[
                    'project_id' => "exists:projects,id|nullable",
                    'start' => "date|nullable",
                    'end' => "date|nullable",
                ];
        } else {
            return back();
        }
      /*
        $rules=[
            'customer_user_id' => request("customer_user_id"),
            'employee_user_id' => request("employee_user_id"),
            'project_id' => request("project_id"),
            'status' => request("status"),

            'start' => request("start"),
            'end' => request("start"),
        ];
 */
        //return $request->validate($rules);
        //return request();
        $rental->update(request()->validate($rules));
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rental  $rental
     * @return \Illuminate\Http\Response
     */
    public function pdf(Rental $rental)
    {
        return view("pages.pdf_rental",compact("rental"));
    }
    public function destroy(Rental $rental)
    {
        //
    }
}
