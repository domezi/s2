<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function rent(\App\Product $product)
    {
        $rental = \App\Rental::find(session("current_rental_id"));
        if($rental==null) {
            $rental = \App\Rental::create(["customer_user_id"=>\Auth::id(),"employee_user_id"=>\Auth::id()]);
            request()->session()->put('current_rental_id', $rental->id);
        }

        $rental->rent($product,request("quantity") || 1);
        
        //return request();
        if(strlen(request("status")) > 4) {
            $rental->set_status($product,request("status"));
            return "created pivot with status ".request("status");
        }

        return back();
    }
  
    public function pivot(\App\Rental $rental, \App\Product $product)
    {
        if(request("notes") != null) {

            $sql="update product_rental set notes= '".request("notes")."' where rental_id = '".$rental->id."' and product_id = '".$product->id."'";
            \DB::statement($sql);

        } else if(request("quantity") != null) {
            $rental->rent($product,request("quantity"));
        }


        return back();

    }

    public function create_and_rent(Request $request) {
        $data=json_decode($request->getContent(),1);
        $product=Product::create($data);
        $this->rent($product);
        return $product;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $products = Product::paginate(15);
        return view("pages.index_products",compact("products"));
    }
    public function create() {
        return view("pages.create_product");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->internStore($request);
        return redirect()->back()->with('success', __("Artikel wurde erfolgreich erfasst.") );
    }

    public function internStore(Request $request)
    {
        $data=$request->validate([
            "title"=>"required|min:4",
            "quantity"=>"required|min:0|numeric",
            "type"=>"in:countable,single,sale|required",
            "qrcode"=>"",
            "purchase_price"=>"numeric|min:0",
            "image_urls"=>"",
            "sale_price"=>"numeric|min:0",
            "daily_rent"=>"numeric|min:0",
            "owner_user_id"=>"numeric|min:0",
            "deposit"=>"numeric|min:0",
            "location_id"=>"exists:locations,id|nullable"
        ]);

        // check if product_single creation will be possible
        if(  substr_count(request("qrcode"),",")   ==    substr_count(request("sns"),",")    ) {
        } else {
            //$validator->getMessageBag()->add('sns', __("Anzahl der Seriennummern und QR-Codes müssen identisch sein."));
            return back()->with('error', __("Anzahl der Seriennummern und QR-Codes müssen identisch sein.") );
        }

        $product= Product::create($data);

        // gibt es mehrere produkte, wie z.b gobos
        // wenn ja => jedes produkt einzeln erzeugen
        if(strstr(request("qrcode"),",")) {
            $qrs = explode(",",request("qrcode"));
            $sns = explode(",",request("sns"));

            foreach($qrs as $index=>$qr) {
                $data=[
                    "qrcode"=>$qr,
                    "sn"=>$sns[$index],
                    "product_id"=>$product->id,
                ];
                $single = \App\ProductSingle::create($data);
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
  

    public function show(Product $product)
    {
        return view("pages.show_product",compact("product"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
     
        $data=$request->validate([
            "title"=>"required|min:4",
            "quantity"=>"required|min:0|numeric",
            //"type"=>"in:countable,single,sale|required",
            "qrcode"=>"",
            //"purchase_price"=>"numeric|min:0",
            //"image_urls"=>"",
            //"sale_price"=>"numeric|min:0",
            "daily_rent"=>"numeric|min:0",
            //"owner_user_id"=>"numeric|min:0",
            //"deposit"=>"numeric|min:0",
            //"location_id"=>"exists:locations,id|nullable"
        ]);

              $product->update($data);
        return redirect()->route("products.show",$product)->with('success', __("Produkt wurde erfolgreich bearbeitet.") );


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit( Product $product)
    {
        //
        return view("pages.edit_product",compact("product"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
