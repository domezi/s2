<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductDetailsController extends Controller
{
    public function scrapeSerp(Request $request)
    {

        $out = array();
        $q= $request->input("q");

        // get price from amazon
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.amazon.de/s?k=".urlencode($q));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);


        $out["aws_images"]=$this->get_strings_between(['data-component-type="s-product-image"','<img src="'],'"',$output);
        $out["aws_prices"]=$this->get_strings_between("a-price-whole\">","<",$output);


        // get price from amazon
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.entireweb.com/images?q=".urlencode($q));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);


        $out["ew_images"]=$this->get_strings_between(['div class="item image-frame" d','data-image-url="'],'"',$output);
        $out["ew_titles"]=$this->get_strings_between(['div class="item image-frame" d','ata-image-name="'],'"',$output);


        return ($out);
    }

    public function get_strings_between($start,$end,$in) {
        $out = array();

        if(is_array($start))
            $ins = explode($start[0],$in);
        else
            $ins = explode($start,$in);
        foreach($ins as $key=>$val) {
            if($key>0) {
                if(is_array($start)) {
                    $val = substr($val, strpos($val, $start[1]));
                    $out[]=explode($end, $val)[1];
                } else {
                    $out[]=explode($end, $val)[0];
                }
            }
        }

        return $out;

    }

}
