<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function qrmode() {
        return view("pages.qrmode");
    }

    public function qrcode_results($qrcode)
    {
        return \App\Product::where("qrcode",$qrcode)->first();
    }

    public function get_current_rental() {

        $rental_id=request()->session()->get('current_rental_id');
        if($rental_id < 1) return null;

        $rental=\App\Rental::find($rental_id)
            ->with(["products","project","employee","customer"])
            ->where("id",$rental_id)->first()->toArray();

        return $rental;

    }

    public function index() {
        $q=request("q");

        if($q == "")
            $q="%";

        /*
        // is it a qr - code at all?
        $json = json_decode($q,0);
        if($json == null)
            $json = json_decode(base64_decode($q),0);
         */


        // is it a qr code?
        $product = \App\Product::where('qrcode', '=',  $q )->first();
        if($product != null)
            return view("pages.show_product",compact("product"));


        // is it a product title ?
        $limit = 10;
        $results = \App\Product::where('title', 'LIKE', '%' . $q . '%')->take($limit)->get();
        return view("pages.searchresults",compact("results"));

    }
}
