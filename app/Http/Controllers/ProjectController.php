<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Gate::allows("rent"))
            $projects = Project::latest()->paginate(15);
        else
            $projects = Project::where('customer_user_id', '=', \Auth::id())->latest()->paginate(15);
        return view("pages.index_projects",compact("projects"));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("pages.create_project", ["locations"=>\App\Location::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $requestData = $request->all();
        if(isset($requestData['create_customer_name'])) {
            $requestData['customer_user_id']=\App\User::create([
                "password"=>\Hash::make(uniqid()),
                "name"=>$requestData["create_customer_name"],
                "email"=>$requestData["create_customer_email"]])->id;
        }
        $request->replace($requestData);

        $data=$request->validate([
            "title"=>"required|min:4",
            "location"=>"nullable|min:4",
            "customer_user_id"=>"exists:users,id|required",
        ]);

        if(!\Gate::allows("rent") && !\Gate::allows("admin"))
            $data["customer_user_id"]=\Auth::id();

        $project= Project::create($data);
        return redirect()->back()->with('success', __("Projekt wurde erfolgreich angelegt.") );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        $locations=\App\Location::all();
        return view("pages.edit_project",compact(["project","locations"]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $data=$request->validate([
            "title"=>"required|min:4",
            "location"=>"nullable|min:4",
            "customer_user_id"=>"exists:users,id|required",
        ]);

        if(!\Gate::allows("rent") && !\Gate::allows("admin"))
            $data["customer_user_id"]=\Auth::id();

        $project->update($data);
        return redirect()->route("projects.index")->with('success', __("Projekt wurde erfolgreich bearbeitet.") );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }
}
