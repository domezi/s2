<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;

class AuthUserCanRent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->can_rent === 0)
            return redirect()->route('login');

        return $next($request);
    }
}
