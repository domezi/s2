@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Diese Zugriffsrechte hast Du:</div>
                <div class="card-body">
                    Ausleihen verwalten: {!! (\Gate::allows("rent"))?"<span style=color:green>Ja</span>" :"<span style=color:red>Nein</span>" !!}<br>
                    Personen/Artikel verwalten: {!! (\Gate::allows("admin"))?"<span style=color:green>Ja</span>" :"<span style=color:red>Nein</span>" !!}<br>
                </div>
            </div>
            <br>
            <div class="card">
                <div class="card-header">Du möchtest etwas mieten?</div>

                @canany(["rent","admin"])
                <div class="card-body">

                    <h2>1. {{__("Du bist Administrator oder Verleiher")}}</h2>
                    <p>{{__("Wir gehen davon aus, das Du eingewiesen wurdest. Deswegen steht hier nichts.")}}</p>

                </div>
                @else
                <div class="card-body">

                    <h2>1. {{__("Projekt anlegen")}}</h2>
                    <p>Das brauchst Du, um Mietverträge zuzuordnen.</p>
                    <a class="btn btn-primary" href="/projects/create">Projekt anlegen ›</a>


                </div>
                <div class="card-body">

                    <h2>2. Mietvertrag entwerfen</h2>
                    <p>Suche über die Suchleiste nach Artikeln, die Du leihen möchtest.
                        Klicke auf '+ Auf aktuelle Liste'. <br/>Falls kein Mietvertrag ausgewählt ist, wird einer im Entwurfsstadium erstellt.</p>
                    <a class="btn btn-primary" href="/products">Alle Artikel ›</a>

                </div>
                @endcanany


            </div>
        </div>
    </div>
</div>
@endsection
