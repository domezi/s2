<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
<!--
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
-->

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom Page Header -->
    @yield('header')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container-fluid">
                <a style="position:absolute" class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'S2') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse container navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <div id="searchbox-wrapper">
                        <div id="searchbox">
                            <form style="display:block;width:100%" action="{{route("search")}}">
                                <input  autofocus="autofocus"
                                                                   value="{{request("q")}}"
                                                      id="q"             
                                class="input" name="q" placeholder="{{ __('Artikel oder Person suchen, oder Label scannen') }}" class="form-control">
                            </form>
                            <form method="get" action="{{route("qrmode")}}">
                                @csrf
                                <button id="qrmode" class="button" >QR</button>
                            </form>
                                <button id="qrcontrol" class="button" >C»</button>
                        </div>
                        <div id="searchbox">
                            <ul class="navbar-nav mr-auto">
                            <?php $menu = [
                                "projects.index"=>"Projekte",
                                "projects.create"=>"Projekt anlegen",
                                "rentals.index"=>"Leihlisten",
                                "products.index"=>"Artikel",
                                "products.create"=>"Artikel erfassen",
                                "locations.index"=>"Locations",
                                "locations.create"=>"Location anlegen",
                                "users.index"=>"Personen",
                                "users.create"=>"Person anlegen",
                            ]; ?>

                            @foreach($menu as $route=>$label)
                            <li class="<?php if($route==Route::currentRouteName()) echo "active" ?> nav-item">
                                    <a class="nav-link" href="<?php echo route($route);?>"><?php echo __($label); ?></a>
                                </li>
                            @endforeach

                        </ul>
                        <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Hallo, {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>


                        </div>
                    </div>
                </div>

                <!-- Right Side Of Navbar -->
                                  </div>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <main class="py-4">
                        @yield('content')
                    </main>
                </div>

                @if(session("current_rental_id"))
                <div class="col-md-3 pt-3">
                    <aside class="card p-4" style="position:relative">

                        <form method="post" style="position:absolute;right:15px;top:20px" action="{{route("rentals.current_close")}}">
                            @csrf
                            <button class="btn btn-outline-success btn-sm">Ausblenden »</button>
                        </form>

                        <?php $rental = null;?>
                        <h1 style="margin-top:-10px;margin-left:0;">{{__("Aktuelle Leihliste")}}</h1>
                        @include('parts.rental') 
                    </aside>
                </div>
                @endif
            </div>
        </div>
    </div>
</body>
</html>
