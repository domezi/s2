<?php
$is_aside=false;
if($rental == null) {
    $is_aside=true;
} 
if(!isset($rental)){
    $rental=\App\Rental::find(session("current_rental_id"));
}
if($rental == null) {
    echo "Keine aktuelle Leihliste.";
    return;
}
?>

<form method="post" action="{{route("rentals.update",$rental->id)}}" onchange="this.submit()">
    @csrf
    @method("put")

    <table class="table table-bordered">
        <tr><td>
        @can('rent')
                Kund*in: </td><td><select name="customer_user_id">
                    <option selected disabled>{{__("Kund*in")}}</option>
                    @foreach(\App\User::all() as $user) 
                    <option
                    <?php if($rental->customer_user_id == $user->id) echo "selected";?>
                        value="{{$user->id}}">
                        {{$user->name}}</option>
                    @endforeach
                </select><br>
                @error("customer_user_id") <div class="text-danger">{{$message}}</div> @enderror

        </tr><tr>

            <td>Verantwortlich:</td><td> <select name="employee_user_id">
                    <option selected disabled>{{__("Verantwortlich")}}</option>
                    @foreach(\App\User::all() as $user) 
                    <option
                    <?php if($rental->employee_user_id == $user->id) echo "selected";?>
                        value="{{$user->id}}">
                        {{$user->name}}</option>
                    @endforeach
                </select><br>
                @error("employee_user_id") <div class="text-danger">{{$message}}</div> @enderror


        </tr><tr><td>
                Projekt: </td><td> <select name="project_id">
                    <option selected disabled>{{__("Projekt")}}</option>
                    @foreach(\App\Project::where('customer_user_id', '=', $rental->customer_user_id)->get() as $project) 
                    <option
                    <?php if($rental->project_id == $project->id) echo "selected";?>
                        value="{{$project->id}}">
                        {{$project->title}}</option>
                    @endforeach
                </select><br>
                @error("project_id") <div class="text-danger">{{$message}}</div> @enderror


        </tr><tr><td>
                Status: </td><td>
                <span class="badge badge-pill badge-{{$rental->color()}}">{{__(ucfirst($rental->status))}}</span>
                <select name="status">
                    <option selected disabled>{{__("Status")}}</option>
                    @foreach( ['draft','reserved','booked','rented','delivered','returned','issue','archive'] as $state) 
                    <option
                    <?php if($rental->status == $state) echo "selected";?>
                        value="{{$state}}">
                        {{__(ucfirst($state))}}</option>
                    @endforeach
                </select>
                <br>
                @error("status") <div class="text-danger">{{$message}}</div> @enderror
            </td>

        </tr><tr><td>


                Ausleihe: </td><td>
                <input type="date" name="start" value="{{$rental->start}}">
                bis
                <input type="date" name="end" value="{{$rental->end}}">
                <br>
                @error("start") <div class="text-danger">{{$message}}</div> @enderror
                @error("end") <div class="text-danger">{{$message}}</div> @enderror


        @else

                Kund*in: </td><td> {{$rental->customer->name}}</td>
        </tr><tr><td>

                Verantwortlich: </td><td> {{($rental->employee != null)? $rental->employee->name : "-"}}</td>

        </tr><tr><td>
                Status: </td><td> {{ucfirst($rental->status)}}
            @if($rental->status == "draft")
                <br/>
                <a href="/rentals/{{$rental->id}}/request_reservation" class="text-info"> <i class="fa fa-external-link"></i> Reservierung beantragen </a>
            @endif
</td>
        </tr><tr><td>

        @can("rent_a_draft",$rental)

                Projekt: </td><td> <select name="project_id">
                    <option selected disabled>{{__("Projekt")}}</option>
                    @foreach(\App\Project::where('customer_user_id', '=', $rental->customer_user_id)->get() as $project) 
                    <option
                    <?php if($rental->project_id == $project->id) echo "selected";?>
                        value="{{$project->id}}">
                        {{$project->title}}</option>
                    @endforeach
                </select><br>
                @error("project_id") <div class="text-danger">{{$message}}</div> @enderror

        </tr><tr><td>
                Ausleihe: </td><td>
                <input type="date" name="start" value="{{$rental->start}}">
                bis
                <input type="date" name="end" value="{{$rental->end}}">
                <br>
                @error("start") <div class="text-danger">{{$message}}</div> @enderror
                @error("end") <div class="text-danger">{{$message}}</div> @enderror

                @else
                Projekt: </td><td> {{$rental->project_title()}}</td></tr>
        <tr><td>{{__("Zeitraum")}}: </td><td> {{$rental->getStartEndHtml()}}
                @endcan
        </tr></td>

                @endcan
        </tr></td>
    </table>


    <textarea class="form-control mb-3" name="notes" placeholder="{{__("Bemerkungen")}}">{{$rental->notes}}</textarea>

    <a href="{{route('rentals.show',$rental)}}"  class="text-success"> <i class="fa fa-external-link"></i>{{__("Leihliste anzeigen")}}</a> </form>
    <a href="{{route('rentals.pdf',$rental)}}" class="text-success"> <i class="fa fa-download"></i> {{__("PDF erzeugen")}}</a> </form>

<hr style="clear:both">
<div class="d-block">
    {{__("Artikel auf dieser Liste:")}}
    <a href="/search?q=%25" class="text-success" style="float:right">{{__("+ Artikel hinzufügen")}}</a>
</div>


@foreach($rental->products as $product) 
<div class="rental-list-item">
    <b><a href="{{route("products.show",$product->id)}}">{{$product->title(40)}}</a>
    </b><br>
    <form method="post" class="d-inline-block" action="{{route("rentals.products.pivot", [$rental->id, $product->id])}}">
        @csrf
        <input name="quantity" value="-1" type="hidden"> 
        <button class="btn btn-secondary"> -</button>
    </form>

    <div class="d-inline-block">
        {{$product->pivot->quantity}} Stck. <br>
        ( {!!$product->getPricingHtml()!!})              
    </div>

    <form method="post" class="d-inline-block" action="{{route("rentals.products.pivot", [$rental->id, $product->id])}}">
        @csrf
        <input name="quantity" value="1" type="hidden"> 
        <button class="btn btn-secondary">+</button>
    </form>

    <form method="post" class="d-inline-block" action="{{route("rentals.products.pivot", [$rental->id, $product->id])}}">
        @csrf
        <textarea class="form-control" onchange="this.form.submit()" placeholder="{{__("Bemerkungen")}}" name="notes" >{{$product->pivot->notes}}</textarea>
    </form>

</div>
@endforeach

<div class="row">
    <div class="col">Tage:</div>
    <div class="col"> {{$rental->days()}} </div>
</div>
<div class="row">
    <div class="col">Tagesmiete</div>
    <div class="col"> {{$rental->currency($rental->daily_rent())}}/Tag </div>
</div>
<div class="row">
    <div class="col">Kaufsummen</div>
    <div class="col"> {{$rental->currency($rental->sale_price())}} </div>
</div>
<hr>

<div class="row">
    <div class="col">Bezahlt</div>
    <div class="col"> {{$rental->currency($rental->payments_sum())}} </div>
</div>
<div class="row">
    <div class="col">Summe total:</div>
    <div class="col"> {{$rental->currency($rental->total_sum())}} </div>
</div>
<div class="row">
    <div class="col">Kaution</div>
    <div class="col"> {{$rental->currency($rental->deposit())}} </div>
</div>
<div class="row">
    <div class="col">Ausstehend:</div>
    <div class="col">
        <b><u> {{$rental->currency($rental->total_sum()-$rental->payments_sum())}}</u></b> 
        <br>
        <a href="/search?q=%25" class="text-success">{{__("+ Zahlung hinzufügen")}}</a>
    </div>
</div>
