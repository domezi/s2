@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <a href="{{route("rentals.index")}}">« {{__("Zurück zu den Ergebnissen")}}</a>

            <div class="card mt-2">

                <div class="card-header">
                <form method="post" style="position:absolute;right:15px;top:20px" action="{{route("rentals.current_open",$rental)}}">
                    @csrf
                    <button class="btn btn-outline-success " >{{__("Als aktuelle Leihliste öffnen")}} »</button>
                </form>
                <h1>{!!$rental->title()!!}</h1>

</div>
<div class="card-body">
                @include('parts.rental') 
</div>

            </div>

        </div>
    </div>
</div>
@endsection
