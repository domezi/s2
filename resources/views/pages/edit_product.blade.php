@extends('layouts.app')

@section('content')

<form method="post">
      @csrf
      @method("put")


      <div class="container">
          <div class="row justify-content-center">
              <div class="col-md-12">
                  <a href="javascript:history.back(2)">« {{__("Zurück zur Produkt Seite (Bearbeiten abbrechen)")}}</a>
                  <br>
                  <br>
                  <div class="row">
                      <div class="col-6">
                          <!--
                              <div class="image">
                              <img src="{{$product->thumb()}}" width=100% style="border-bottom:1px solid gray;" id="big-image">
                              <div class="tiny-images">
                              @foreach(explode(",", $product->image_urls) as $image)
                              <img src="{{$image}}" height="70" onclick="document.getElementById('big-image').src='{{$image}}'">
                              @endforeach
                              </div>
                              </div>

                          -->
                          <div id="carouselExampleCaptions" class="carousel carousel-thumbnails" data-ride="carousel">
                              <div class="carousel-inner">
                                  @foreach (explode(",", $product->image_urls) as $image)
                                  <div class="carousel-item @if ($loop->index == 0) active @endif">
                                      <img src="{{$image}}" class="d-block img-fluid" alt="..." >
                                  </div>
                                      @endforeach
                              </div>
                              <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                  <span class="sr-only">Previous</span>
                              </a>
                              <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                  <span class="sr-only">Next</span>
                              </a>
                              <ol class="carousel-indicators">
                                  @foreach ( explode(",", $product->image_urls) as $image )
                                  <li data-target="#carouselExampleCaptions" data-slide-to="{{$loop->index}}" @if ($loop->index == 0) class="active" @endif> <img class="d-block w-100" src="{{$image}}"
                                                                                                                                                                                        class="img-fluid"></li>
                                  @endforeach
                              </ol>
                          </div>
                      </div>
                      <div class="col-6">

                        <input value="{{$product->title}}" class="form-control" name="title" value="{{old("title")}}" placeholder="{{ __("Titel") }}">
                        <label>{{__("Titel")}}</label>
                        @error("title") <span class="text-danger">{{$message}}</span> @enderror


                          <div class="row">
                              <div class="col">Tagesmiete<div >

                                  <input value="{{$product->daily_rent}}" class="form-control" name="daily_rent" 
                                   placeholder="{{ __("Tagesmiete") }}">
                                  <label>{{__("Tagesmiete")}}</label>
                                  @error("daily_rent") <span class="text-danger">{{$message}}</span> @enderror


                                  </div></div>
                                  <div class="col">Verfügbar<div>
                                      
                                  <input value="{{$product->quantity}}" class="form-control" name="quantity" 
                                   placeholder="{{ __("Menge") }}">
                                  <label>{{__("Menge")}}</label>
                                  @error("quantity") <span class="text-danger">{{$message}}</span> @enderror


                                      
                                      </div></div>
                          </div>

                          <div class="detail-area">
                              <table class="table table-bordered">
                                  <tr><td>{{__("S/N")}}:</td><td>
                                          @foreach($product->singlesDepricated() as $single)
                                          <span class="badge badge-secondary">{{$single->sn}}
                                              <a class="btn btn-sm btn-warning" href="#">{{__("Löschen")}}</a>
                                          </span>
                                          @endforeach


                                          <a class="btn btn-sm btn-link" href="#">Weitere S/N hinzufügen</a>
                                      </td>
                                  </tr>
                                  <tr><td>{{__("QR-Code")}}:</td><td>

                                                         <input value="{{$product->qrcode}}" class="form-control" name="qrcode" 
                                   placeholder="{{ __("QR Code") }}">
                                  <label>{{__("QR Code")}}</label>
                                  @error("qrcode") <span class="text-danger">{{$message}}</span> @enderror



                                      
                                      </td></tr>
                                  <tr><td>{{__("Location")}}:</td><td>{{ $product->location == null  ? "-": $product->location->title }}</td></tr>
                                  <tr><td>{{__("Eigentümer*in")}}:</td><td>{{ $product->owner == null  ? "-": $product->owner->name }}</td></tr>
                              </table>
                          </div>

                      </div>

                  </div>

                        <input value="{{ __("Änderungen übernehmen") }}" type="submit" class="btn btn-primary">
</form>

              </div>
          </div>
      </div>
@endsection
