@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
             
            @can("rent")
            <h1>{{__("Alle Projekte")}}</h1>
            @else
            <h1>{{__("Meine Projekte")}}</h1>
            @endcan

            @foreach($projects as $project)
            <div class="card mb-3">
                {{$project->displaySearchresult()}}
            </div>
            @endforeach

            {{$projects->links()}}

        </div>
    </div>
</div>
@endsection
