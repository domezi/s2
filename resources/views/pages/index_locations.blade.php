@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
             
            <h1>{{__("Alle locations")}}</h1>

            @foreach($locations as $location)
            <div class="card mb-3 pt-3 pb-1  hoverline">
                <h3 class="px-3">{{$location->title}}</h3>
                <div class="px-3">
                    {{$location->description}}
                </div>
                <br>

                <div class="border-top px-3 mt-2 pt-2">
                    {{__("Kontaktperson")}}: {{$location->contact->name}}
                </div>
                <div class="px-3 border-top mt-2">

                    <form method=get class='d-inline-block py-2' action='{{route("locations.edit",$location)}}'>
                        <input type=submit class='btn btn-link btn-sm ' value='{{__("Location bearbeiten")}}'>
                    </form>

                </div>
            </div>
            @endforeach

            {{$locations->links()}}

        </div>
    </div>
</div>
@endsection
