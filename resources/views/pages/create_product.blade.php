@extends('layouts.app')

@section('header')
    <script src="{{ asset('js/create_product.js') }}"> </script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
          
            @if(session()->has('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>
            @endif


            @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
            @endif


            <div class="card">
                <div class="card-header">
                   <h1>{{ __('Artikel erfassen') }}</h1>
</div>
                <div class="card-body">
                    <form method="post">
                        @csrf
                        <div class="form-row">
                            <input class="form-control" autofocus="autofocus" tabindex="0" name="title" value="{{old('title')}}" placeholder="{{ __("Titel") }}">
                            <label>{{__("Titel")}}</label>
                            @error("title") <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                        <div class="form-row">
                            <div class="form-group col-6">
                                <div class="form-row">
                                    <textarea class="form-control" value="" name="image_urls" id="image_urls" oninput="update_images()" placeholder="{{ __("Bilder URLs, durch Kommata getrennt") }}">{{old('image_urls')}}</textarea>

                                    <label>{{__("Bilder URLs, durch Kommata getrennt")}}</label>
                                    @error("image_urls") <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-6">
                                        <img id="big_image" class="" src="https://ppc.tools/wp-content/themes/ppctools/img/no-thumbnail.jpg" height="150" >
                                    </div>
                                    <div class="form-group col-6 jumbotron-fluid">
                                        <div id="tiny_images" class="tiny-images">
                                            <img class="img-thumbnail" src="https://ppc.tools/wp-content/themes/ppctools/img/no-thumbnail.jpg" height="70" width="70">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <input type="number" value="{{intval(old('quantity',1))}}" value=1 class="form-control" name="quantity" placeholder="{{ __("Verfügbare Menge") }}">
                                <label>{{__("Verfügbare Menge")}}</label>
                                @error("quantity") <span class="text-danger">{{$message}}</span> @enderror
                                
                                
                                <select class="custom-select" onchange="
                                                            var sns=document.getElementById('sns_wrap');
                                                            var sale_price=document.getElementById('sale_price_wrap');
                                                            var daily_rent=document.getElementById('daily_rent_wrap');
                                                            var deposit=document.getElementById('deposit_wrap');
                                        if(this.value == 'single')sns.style.display='block'; else sns.style.display='none';
                                        if(this.value != 'countable')sale_price.style.display='block'; else sale_price.style.display='none';
                                        if(this.value != 'sale')daily_rent.style.display='block'; else daily_rent.style.display='none';
                                        if(this.value != 'sale')deposit.style.display='block'; else deposit.style.display='none';
                                        " name="type">
                                    <option value="single">{{ __("Einzelne Artikel (Serien-Nummer)") }}</option>
                                    <option value="countable">{{ __("Abzählware (Gobos, Uniklemmen)") }}</option>
                                    <option value="sale">{{ __("Zum Verkauf (Gaffa, Beleuchterklemmen)") }}</option>
                                </select>
                                <label>{{__("Artikel Typ")}}</label>
                                @error("type") <span class="text-danger">{{$message}}</span> @enderror

                                <input class="form-control" value="{{old('qrcode')}}"  name="qrcode" placeholder="{{ __("QR-Code auf Label (bei SN durch Kommata getrennt)") }}">
                                <label>{{__("QR-Code auf Label (bei SN durch Kommata getrennt)")}}</label>
                                @error("qrcode") <span class="text-danger">{{$message}}</span> @enderror
                            

                                <div id="sns_wrap">
                                    <input class="form-control" value="{{old('sns')}}" id="sns" name="sns" placeholder="{{ __("Seriennummern (durch Kommata gertrennt)") }}">
                                    <label>{{__("Seriennummern (durch Kommata getrennt)")}}</label>
                                    @error("sns") <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-row">     
                            <div class="form-group col-6">
                                <div class="form-row">              
                                    <div class="form-group col-6">
                                        <div id="daily_rent_wrap">
                                        <input type="number" value="{{old('daily_rent')}}" id="daily_rent" value=0 min="0" step="0.001" class="form-control" name="daily_rent" placeholder="{{ __("Tagesmiete") }}">
                                        <label>{{__("Tagesmiete")}}</label>
                                        @error("daily_rent") <span class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    
                                        <input type="number" value="{{old('purchase_price')}}" value=0
onchange="document.getElementById('daily_rent').value=Math.round(this.value/6)/10;document.getElementById('deposit').value=(this.value/10)"
 min="0" step="0.001" class="form-control" name="purchase_price" placeholder="{{ __("Ursprünglicher Kaufpreis") }}">
                                        <label>{{__("Ursprünglicher Kaufpreis")}}</label>
                                        @error("purchase_price") <span class="text-danger">{{$message}}</span> @enderror
                                    </div>

                                    <div class="form-group col-6">
                                        <div id="deposit_wrap">
                                        <input type="number" value="{{old('deposit')}}" id="deposit" value=0 min="0" step="0.001" class="form-control" name="deposit" placeholder="{{ __("Kaution") }}">
                                        <label>{{__("Kaution")}}</label>
                                        @error("deposit") <span class="text-danger">{{$message}}</span> @enderror
                                        </div>

                                        <div id="sale_price_wrap">
                                        <input type="number" value="{{floatval(old('sale_price'))}}" id="sale_price" value=0 min="0" step="0.001" class="form-control" name="sale_price" placeholder="{{ __("Verkaufspreis") }}">
                                        <label>{{__("Verkauspreis")}}</label>
                                        @error("sale_price") <span class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <select class="custom-select" name="location_id">

                                    <option selected disabled>{{__("Location")}}</option>
                                    @foreach(\App\Location::all() as $location) 
                                    <option
                                        value="{{$location->id}}">
                                        {{$location->title}}</option>
                                    @endforeach

                                </select>
                                <label>{{__("Location")}}</label>
                                @error("location_id") <span class="text-danger">{{$message}}</span> @enderror

                                <select class="custom-select" name="owner_user_id">

                                    @foreach(\App\User::all() as $user) 
                                        <option
                                        value="{{$user->id}}">
                                        {{$user->name}}
                                            </option>
                                    @endforeach

                                </select>
                                <label>{{__("Eigentümer*in")}}</label>
                                @error("owner_user_id") <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                                            
                        <br>
                        <input value="{{ __("Artikel erfassen") }}" type="submit" class="btn btn-primary">

                    </form>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
