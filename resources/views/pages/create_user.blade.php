@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
          
            @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
            @endif

            <div class="card">
                <div class="card-header"><h1>{{ __('Person anlegen') }}</h1></div>

                <div class="card-body">

                  
                    <form method="post">
                        @csrf

                        <input class="form-control" autofocus name="fname" placeholder="{{ __("Vorname") }}">
                        <label>{{__("Vorname")}}</label>
                        @error("fname") <span class="text-danger">{{$message}}</span> @enderror


                        <input class="form-control" name="lname" placeholder="{{ __("Nachname") }}">
                        <label>{{__("Nachname")}}</label>
                        @error("lname") <span class="text-danger">{{$message}}</span> @enderror

                        <input class="form-control" name="email" placeholder="{{ __("Email") }}">
                        <label>{{__("Email")}}</label>
                        @error("email") <span class="text-danger">{{$message}}</span> @enderror

                        <br>
                        <input value="{{ __("Person anlegen") }}" type="submit" class="btn btn-primary">

                    </form>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
