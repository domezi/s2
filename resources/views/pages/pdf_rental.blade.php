<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<style>
hr.dotted {
    border-top:2px dotted #ddd;
    margin:35px 0;
}
</style>
<div class="container mt-4">

    <div class="alert alert-info">
        @if($rental->status == "draft")
            Dieses Dokument ist ein Entwurf einer Leihliste und hat keinerlei Rechtsgülitgkeit.
        @elseif($rental->status == "reserved")
            Dieses Dokument ist eine Reservierungsbestätigung.
        @elseif($rental->status == "booked")
            Dieses Dokument ist eine Buchungsbestätigung.
        @elseif($rental->status == "rented")
            Dieses Dokument ist ein Mietvertrag und bescheinigt die Übergabe der aufgelisteten Artikel.
        @elseif($rental->status == "delivered")
            Dieses Dokument ist ein Mietvertrag und bescheinigt die Entgegennahme der aufgelisteten Artikel durch den Kunden.
        @elseif($rental->status == "returned")
            Dieses Dokument bescheinigt die Rücknahme der aufgelisteten Artikel durch die verleihende Person.
        @else
            Kein bekannter Leihlistenstatus.
        @endif
    </div>
    <h1>Mietvertrag</h1>
<?php
//print_r($rental->employee);
?>

<p>
{{$rental->employee->address() == null ? "Keine Adreses angegeben.":$rental->employee->address()->displaySearchresult()}}
<br>
Verleih an: {!!$rental->customer->displaySearchresult()!!}
<br>
Rental: ID {{$rental->id}}, erstellt {{$rental->created_at}}, verändert {{$rental->updated_at}}
</p>


    <table class="table table-sm table-bordered">
        <tr><td> Verantwortlich: </td><td>{{$rental->employee->name}}</td></tr>
        <tr><td> Zeitraum: </td><td>{{$rental->getStartEndHtml()}}</td></tr>
        <tr><td> Status: </td><td>{{$rental->status()}}</td></tr>
        <tr><td> Projekt: </td><td>{{$rental->project_title()}}</td></tr>
    </table>


    <p style="white-space:pre-wrap">{{$rental->notes}}</p>
    <hr class="dotted">

    <h1>Artikel</h1>

    <table border="1" width="500" class="table table-sm table-striped table-bordered">
        <tr>
            <th>Artikel</th>
            <th>Bemerkungen</th>
            <th>Menge</th>
            <th>Preis</th>
        </tr>
        @foreach($rental->products as $product) 
        <tr>
            <td>{{$product->title(400)}}</td>
            <td> {{$product->pivot->notes}} </td>
            <td> {{$product->pivot->quantity}} Stck. </td>
            <td> {!!$product->getPricingHtml()!!}</td>
        </tr>
        @endforeach
    </table>

    <hr class="dotted">

    <h1>Abrechnung</h1>

<div class="row">
    <div class="col">Tage:</div>
    <div class="col"> {{$rental->days()}} </div>
</div>
<div class="row">
    <div class="col">Tagesmiete</div>
    <div class="col"> {{$rental->currency($rental->daily_rent())}}/Tag </div>
</div>
<div class="row">
    <div class="col">Kaufsummen</div>
    <div class="col"> {{$rental->currency($rental->sale_price())}} </div>
</div>
<hr>

<div class="row">
    <div class="col">Bezahlt</div>
    <div class="col"> {{$rental->currency($rental->payments_sum())}} </div>
</div>
<div class="row">
    <div class="col">Summe total:</div>
    <div class="col"> {{$rental->currency($rental->total_sum())}} </div>
</div>
<div class="row">
    <div class="col">Kaution</div>
    <div class="col"> {{$rental->currency($rental->deposit())}} </div>
</div>
<div class="row">
    <div class="col">Ausstehend:</div>
    <div class="col">
        <b><u> {{$rental->currency($rental->total_sum()-$rental->payments_sum())}}</u></b> 
        <br>
    </div>
</div>
</div>
