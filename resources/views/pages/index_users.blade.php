@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
             
            <h1>{{__("Alle Personen")}}</h1>

            <div class="alert alert-info">{{__("Um Kunden anzulegen, bitte ausloggen und auf registrieren klicken.")}}</div>

            @foreach($users as $user)
            <div class="card mb-3 py-3 px-4 d-block hoverline">
<h2>
                {{$user->name}} 
</h2>
                <div>
                    {{$user->email}}  
                </div>
                <div>
                    {{$user->phone}} 
                </div>
                <div>
                    Addresse:
                    <form method="post" onchange="this.submit()" class="border-top mt-2 pt-2"  action="{{route("users.update",$user)}}">
                    @csrf
                    @method("put")
                        <input name="create_address" class="form-control" placeholder="Addresse erstellen (Strasse und Nr, Stadt und Ort)">
                        <select name="default_address_id">
                            <option disabled selected>Standard Adresse auswählen</option>
                            @foreach(\App\Address::all() as $address) 
                            <option value="{{$address->id}}" @if($address->id==$user->default_address_id) selected @endif >{{$address->displaySearchresult()}}</option>
                            @endforeach
                        </select>
                    </form>
                </div>
                <form method="post" onchange="this.submit()" class="border-top mt-2 pt-2"  action="{{route("users.update",$user)}}">
                    @csrf
                    @method("put")
                    Admin:
                    <select name="can_admin">
                        <option value="1" @if($user->can_admin) selected @endif >Ja</option>
                        <option value="0" @if(!$user->can_admin) selected @endif >Nein</option>
                    </select>
                    Rent:
                    <select name="can_rent">
                        <option value="1" @if($user->can_rent) selected @endif >Ja</option>
                        <option value="0" @if(!$user->can_rent) selected @endif >Nein</option>
                    </select>

                </form>
            </div>
            @endforeach

            {{$users->links()}}
        </div>
    </div>
</div>
@endsection
