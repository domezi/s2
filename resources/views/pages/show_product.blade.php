@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
                <a href="javascript:history.back(-1)">« {{__("Zurück zu den Ergebnissen")}}</a>
                <a href="{{route('products.edit',$product)}}">| {{__("Produkt bearbeiten")}}</a>
                <br>
                <br>
            <div class="row">
                <div class="col-6">
                    <!--
                    <div class="image">
                        <img src="{{$product->thumb()}}" width=100% style="border-bottom:1px solid gray;" id="big-image">
                        <div class="tiny-images">
                        @foreach(explode(",", $product->image_urls) as $image)
                            <img src="{{$image}}" height="70" onclick="document.getElementById('big-image').src='{{$image}}'">
                        @endforeach
                        </div>
                    </div>
                
                -->
                    <div id="carouselExampleCaptions" class="carousel carousel-thumbnails" data-ride="carousel">
                        <div class="carousel-inner">
                            @foreach (explode(",", $product->image_urls) as $image)
                            <div class="carousel-item @if ($loop->index == 0) active @endif">
                            <img src="{{$image}}" class="d-block img-fluid" alt="..." >
                            </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        <ol class="carousel-indicators">
                            @foreach ( explode(",", $product->image_urls) as $image )
                            <li data-target="#carouselExampleCaptions" data-slide-to="{{$loop->index}}" @if ($loop->index == 0) class="active" @endif> <img class="d-block w-100" src="{{$image}}"
                                class="img-fluid"></li>
                            @endforeach
                        </ol>
                    </div>
                </div>
                <div class="col-6">
                        <h1>{{$product->title}}</h1>
                        <p>{!!$product->getOwnerHtml()!!}</p>
                        <div class="row">
                            <div class="col">Tagesmiete<div class="big">{!!$product->getPricingHtml()!!}</div></div>
                            <div class="col">Verfügbar<div class="big">{!!$product->getQuantityAvailableCurrent()!!}
                                    /
                            {!!$product->getQuantityHtml()!!}</div></div>
                        </div>

                        <div class="detail-area">
                            <table class="table table-bordered">
                                <tr><td>{{__("S/N")}}:</td><td>
                                @foreach($product->singlesDepricated() as $single)
                                <span class="badge badge-secondary">{{$single->sn}}</span>
                                @endforeach
                                </td>
                                </tr>
                                <tr><td>{{__("QR-Code")}}:</td><td>{{ $product->qrcode }}</td></tr>
                                <tr><td>{{__("Location")}}:</td><td>{{ $product->location == null  ? "-": $product->location->title }}</td></tr>
                                <tr><td>{{__("Eigentümer*in")}}:</td><td>{{ $product->owner == null  ? "-": $product->owner->name }}</td></tr>
                            </table>
                        </div>

                        <div class="rent-area">
                            <form method="post" class="d-flex" action="{{route("products.rent", $product->id)}}">
                                @csrf
                                <input name="quantity" value="1" class="bigger form-control" type="number" style="width:80px"> 
                                <button class="btn btn-primary bigger">+ {{__("Auf aktuelle Liste")}}</button>
                            </form>
                        </div>
                        @cannot("rent")
                        <div class="text-danger">{{__("Du kannst diesen Artikel nur auf Listen mit dem Status \"Draft\" hinzufügen.")}}</div>
                        @endcannot
                </div>
                
            </div>

            <br>
                <div class="card">
                    <div class="card-header" style="border-bottom:0">
                        {{__("Dieser Artikel kommt in folgenden Leihlisten vor")}}:
                    </div>
                    @foreach($product->rentals() as $rental) 
                        {!!$rental->displaySearchresult()!!}
                    @endforeach
            </div>

        </div>
    </div>
</div>
@endsection
