@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
          
            @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
            @endif

            <div class="card">
                <div class="card-header"><h1>{{ __('Projekt anlegen') }}</h1></div>

                <div class="card-body">

                  
                    <form method="post">
                        @csrf

                        <input class="form-control" name="title" placeholder="{{ __("Titel") }}">
                        <label>{{__("Titel")}}</label>
                        @error("title") <span class="text-danger">{{$message}}</span> @enderror

                        <input list="locations" class="form-control" name="location" placeholder="{{ __("Ort, an dem gedreht wird bzw. Versandadresse") }}">
                        <label>{{__("Ort, an dem gedreht wird bzw. Versandadresse")}}</label>
                        @error("location") <span class="text-danger">{{$message}}</span> @enderror

                        <datalist id="locations">
                            @foreach($locations as $location)
                            <option>{{$location->title}} - {{$location->description}}</option>
                            @endforeach
                        </datalist>

                        <div class="row ">
                            <div class="col">
                                @canany(["admin","rent"])
                                <select class="custom-select" name="customer_user_id">
                                    <option selected>{{__("Kund*in erstellen (rechte Felder nutzen)")}}</option>
                                    @foreach(\App\User::all() as $user) 
                                    <option
                                        @if($user->id == old("customer_user_id"))
                                        selected="selected"
                                        @endif
                                        value="{{$user->id}}">
                                        {{$user->name}}</option>
                                    @endforeach
                                </select>
                                <label>{{__("Kund*in auswählen")}}</label>
                                @error("customer_user_id") <span class="text-danger">{{$message}}</span> @enderror
                                @else
                                <input type=hidden name="customer_user_id" value="{{\Auth::id()}}">
                                @endcanany
                            </div>
                            <div class="col">
                                <input class="form-control" placeholder="Name" name="create_customer_name">
                                <input class="form-control" placeholder="Email" name="create_customer_email">
                                <label>{{__("Oder erstelle ein Kund*innenkonto")}}</label>
                            </div>
                        </div>

                        <br>
                        <input value="{{ __("Projekt anlegen") }}" type="submit" class="btn btn-primary">

                    </form>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
