@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
          
            @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
            @endif

            <div class="card">
                <div class="card-header"><h1>{{ __('Projekt bearbeiten') }}</h1></div>

                <div class="card-body">

                  
                    <form method="post">
                        @csrf
                        @method("put")

                        <input class="form-control" name="title" value="{{$project->title}}" placeholder="{{ __("Titel") }}">
                        <label>{{__("Titel")}}</label>
                        @error("title") <span class="text-danger">{{$message}}</span> @enderror


                        <input list="locations" class="form-control" value="{{$project->location}}" name="location" placeholder="{{ __("Ort, an dem gedreht wird bzw. Versandadresse") }}">
                        <label>{{__("Ort, an dem gedreht wird bzw. Versandadresse")}}</label>
                        @error("location") <span class="text-danger">{{$message}}</span> @enderror

                        <datalist id="locations">
                            @foreach($locations as $location)
                            <option>{{$location->title}} - {{$location->description}}</option>
                            @endforeach
                        </datalist>


                        @canany(["admin","rent"])
                        <select class="custom-select" name="customer_user_id">
                            <option selected disabled>{{__("Kund*in")}}</option>
                            @foreach(\App\User::all() as $user) 
                            <option
                                @if($user->id == $project->customer_user_id)
                                selected="selected"
                                @endif
                                value="{{$user->id}}">
                                {{$user->name}}</option>
                            @endforeach
                        </select>
                        <label>{{__("Eigentümer")}}</label>
                        @error("customer_user_id") <span class="text-danger">{{$message}}</span> @enderror
                        @else
                        <input type=hidden name="customer_user_id" value="{{\Auth::id()}}">
                        @endcanany

                        <br>
                        <input value="{{ __("Änderungen am Projekt übernehmen") }}" type="submit" class="btn btn-primary">

                    </form>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
