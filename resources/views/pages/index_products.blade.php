@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
             
            <h1>{{__("Alle Artikel")}}</h1>
           
            @foreach($products as $product)
                {{$product->displaySearchresult()}}
            @endforeach

            {{$products->links()}}
        </div>
    </div>
</div>
@endsection
