@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
          
            @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
            @endif

            <div class="card">
                <div class="card-header"><h1>{{ __('Location anlegen') }}</h1></div>

                <div class="card-body">

                  
                    <form method="post">
                        @csrf

                        <input class="form-control" name="title" value="{{old("title")}}" placeholder="{{ __("Titel") }}">
                        <label>{{__("Titel")}}</label>
                        @error("title") <span class="text-danger">{{$message}}</span> @enderror


                        <input class="form-control" name="description" value="{{old("description")}}" placeholder="{{ __("Beschreibung") }}">
                        <label>{{__("Beschreibung")}}</label>
                        @error("description") <span class="text-danger">{{$message}}</span> @enderror


                        <select class="custom-select" name="contact_user_id">
                            <option selected disabled>{{__("Kontaktperson")}}</option>
                            @foreach(\App\User::all() as $user) 
                            <option
                                value="{{$user->id}}">
                                {{$user->name}}</option>
                            @endforeach
                        </select>
                        <label>{{__("Kontaktperson")}}</label>
                        @error("contact_user_id") <span class="text-danger">{{$message}}</span> @enderror

                        <br>
                        <input value="{{ __("Location anlegen") }}" type="submit" class="btn btn-primary">

                    </form>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
