@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
             
            @can("rent")
            <h1>{{__("Alle Leihlisten")}}</h1>
            @else
            <h1>{{__("Meine Leihlisten")}}</h1>
            @endcan

            <div class='alert alert-info'>{{__("Eine Leihliste erstellst Du, indem Du nach einem Artikel suchst, und den Button \"+ Auf aktuelle Liste\" klickst.")}}</div>

            <div class="card">
                @foreach($rentals as $rental)
                    @if($rental->status !== "archive")
                    {!!$rental->displaySearchresult()!!}
                    @endif
                @endforeach
            </div>
            {{$rentals->links()}}


        </div>
    </div>
</div>
@endsection
