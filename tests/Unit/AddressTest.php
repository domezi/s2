<?php

namespace Tests\Unit;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

//use PHPUnit\Framework\TestCase;
use Tests\TestCase;

class AddressTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testAddressFactoryCreation()
    {

       $address= factory(\App\Address::class)->create();
       $this->assertNotNull($address->firstname);

       
    }

    public function testAddressCreation()
    {

        \App\Address::create([
            'firstname'=>"sally",
            'lastname'=>"sally",
            'created_by_user_id'=>factory(\App\User::class)->create()->id
        ]);

        $this->assertDatabaseHas('addresses', [
            'firstname' => 'sally',
        ]);

    }
}
