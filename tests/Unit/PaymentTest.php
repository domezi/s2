<?php

namespace Tests\Unit;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

//use PHPUnit\Framework\TestCase;
use Tests\TestCase;

class PaymentTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic functional test example.
     *
     * @return void
     */
  

    public function testPaymentFactoryCreation()
    {

        /*
        $p=factory(\App\Project::class)->create();
        $r=factory(\App\Rental::class)->create(["project_id"=>$p->id]);
        */

        $pay=factory(\App\Payment::class)->create();

        $this->assertNotNull($pay);
        $this->assertNotNull($pay->rental->id);
        $this->assertNotNull($pay->project->id);

    }

  }
