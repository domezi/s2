<?php

namespace Tests\Unit;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

//use PHPUnit\Framework\TestCase;
use Tests\TestCase;

class ProductTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic functional test example.
     *
     * @return void
     */
  

   
    public function test_a_product_is_available() {
        $p=factory(\App\Product::class)->create(["quantity"=>3]);
        $r=factory(\App\Rental::class)->create(["start"=>"2020-01-03","end"=>"2020-01-05"]);
        $r->rent($p,2);

        $this->assertEquals(3,$p->getQuantityAvailable("2020-01-01","2020-01-02"));
        $this->assertEquals(3,$p->getQuantityAvailable("2020-01-01","2020-01-03"));
        $this->assertEquals(3,$p->getQuantityAvailable("2020-01-05","2020-01-08"));
        $this->assertEquals(1,$p->getQuantityAvailable("2020-01-04","2020-01-08"));
        $this->assertEquals(1,$p->getQuantityAvailable("2020-01-01","2020-01-04"));
        $this->assertEquals(1,$p->getQuantityAvailable("2020-01-03","2020-01-09"));
        $this->assertEquals(1,$p->getQuantityAvailable("2020-01-04","2020-01-04"));

        $r->rent($p,1);
        $this->assertEquals(3,$p->fresh()->getQuantityAvailable("2020-01-01","2020-01-03"));
        $this->assertEquals(0,$p->fresh()->getQuantityAvailable("2020-01-04","2020-01-04"));

    }


    public function test_product_creation()
    {

        $u=factory(\App\User::class)->create();
        $p=factory(\App\Product::class)->create(["owner_user_id"=>$u->id]);
        $this->assertNotNull($p->owner);

        //check key constraint
        //$u->delete();
        //$this->assertNull($p->fresh());

    }

  }
