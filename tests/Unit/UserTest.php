<?php

namespace Tests\Unit;

//use PHPUnit\Framework\TestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample() {
        $this->assertTrue(true);
    }

    public function testDatabase() {

        \App\User::create([
            'name'=>"sally",
            'password'=>"sally",
            'email' => "sally@example.com"
        ]);


        $this->assertDatabaseHas('users', [
            'email' => 'sally@example.com',
        ]);
    }
 

  public function test_user_can_change_the_default_address() {

        $user=\App\User::create([
            'name'=>"sally",
            'password'=>"sally",
            'email' => "sally@example.com",
            'default_address_id'=>null
        ]);

        $address2=factory(\App\Address::class)->create();
        $this->assertNotEquals($user->default_address_id,$address2->id);

        $user->setDefaultAddress($address2);
        $this->assertEquals($user->default_address_id,$address2->id);


    }

  public function test_user_has_a_default_address() {

        $a=factory(\App\Address::class)->create();
        $this->assertNotNull($a);
        $this->assertIsInt($a->id);

        $user=\App\User::create([
            'name'=>"sally",
            'password'=>"sally",
            'email' => "sally@example.com",
            'default_address_id'=>$a["id"]
        ]);
        
        $this->assertNotNull($user->address());



    }


    public function test_a_user_can_have_projects() {
        $u=factory(\App\User::class)->create();
        $this->assertEquals(0,count($u->projects));
        $p=factory(\App\Project::class)->create(["customer_user_id"=>$u->id]);
        $u->projects()->save($p);
        $this->assertEquals(1,count($u->fresh()->projects)); // fails, project hasn't been added

    }

    public function test_a_user_model_can_create_a_project() {
        $u=factory(\App\User::class)->create();
        $u->createProject();
        $this->assertEquals(1,count($u->fresh()->projects));
    }

}
