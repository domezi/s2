<?php

namespace Tests\Unit;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

//use PHPUnit\Framework\TestCase;
use Tests\TestCase;

class RentalTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic functional test example.
     *
     * @return void
     */
  

    public function test_is_self_contained()
    {

        $p=factory(\App\Project::class)->create();
        $u1=factory(\App\User::class)->create();

        $rental= factory(\App\Rental::class)->create(["customer_user_id"=>$u1->id]);
        $this->assertFalse($rental->isSelfContainedRental());

        //$rental->employee()->save($u1);
        //$rental->employee_user_id=$u1->id;$rental->save();
        $rental->employee()->associate($u1)->save();
        $this->assertFalse($rental->isSelfContainedRental());

    }

    public function test_rental_days()
    {

        $rental = factory(\App\Rental::class)->create(["start"=>"2020-01-01","end"=>"2020-01-04"]);

        $this->assertEquals(3,$rental->days());

    }

    public function test_rental_has_products()
    {

        $products = factory(\App\Product::class,3)->create();
        $rental = factory(\App\Rental::class)->create();

        $this->assertEquals(0,count($rental->fresh()->products));

        $rental->rent($products[1]);
        $this->assertEquals(1,count($rental->fresh()->products));

    }


    public function testRentalFactoryCreation()
    {

        $p=factory(\App\Project::class)->create();
        $rental= factory(\App\Rental::class)->create(["project_id"=>$p->id]);
        $this->assertNotNull($rental->project);

        //check key constraint
        $p->delete();
        $this->assertNull($rental->fresh());


    }

    public function test_a_rental_has_payments()
    {

        $p=factory(\App\Payment::class)->create(["sum"=>80]);
        $p2=factory(\App\Payment::class)->create(["sum"=>70]);
        $rental= factory(\App\Rental::class)->create();

        $rental->payments()->save($p);
        $rental->payments()->save($p2);
        $this->assertEquals(150,$rental->payments_sum());

    }


    public function test_rental_deposit_sum()
    {

        $p1=factory(\App\Product::class)->create(["deposit"=>40,"daily_rent"=>30,"sale_price"=>200]);
        $p2=factory(\App\Product::class)->create(["deposit"=>60,"daily_rent"=>10]);
        $rental= factory(\App\Rental::class)->create();

        $rental->rent($p1,2);
        $this->assertEquals(80,$rental->deposit());
        $this->assertEquals(60,$rental->daily_rent());

        $rental->rent($p2,1);

        $this->assertEquals(140,$rental->fresh()->deposit());
        $this->assertEquals(70,$rental->fresh()->daily_rent());
        $this->assertEquals(400,$rental->fresh()->sale_price());

    }

  }
