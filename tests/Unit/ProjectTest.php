<?php

namespace Tests\Unit;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

//use PHPUnit\Framework\TestCase;
use Tests\TestCase;

class ProjectTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic functional test example.
     *
     * @return void
     */
    
    public function testAddressCreation()
    {

        \App\Project::create([
            'title'=>"sally",
            'customer_user_id'=>factory(\App\User::class)->create()->id
        ]);

        $this->assertDatabaseHas('projects', [
            'title' => 'sally',
        ]);

    }

    public function test_rental_deposit_sum()
    {

        $p1=factory(\App\Product::class)->create(["deposit"=>40,"daily_rent"=>30,"sale_price"=>200]);
        $p2=factory(\App\Product::class)->create(["deposit"=>60,"daily_rent"=>10]);
        $rental1= factory(\App\Rental::class)->create();

        $rental2= factory(\App\Rental::class)->create();
        $rental2->rent($p1,3);
        $rental1->rent($p1,2);

        $project = factory(\App\Project::class)->create();
        $project->rentals()->save($rental1);
        $project->rentals()->save($rental2);

        $this->assertEquals(200,$project->fresh()->deposit());
        $this->assertEquals(150,$project->fresh()->daily_rent());
        $this->assertEquals(1000,$project->fresh()->sale_price());

    }


    public function test_a_projects_belongs_to_a_user()
    {

        $u=factory(\App\User::class)->create();
        $p=\App\Project::create([
            'title'=>"sally",
            'customer_user_id'=>$u->id
        ]);
        $this->assertEquals($u->id,$p->owner->id);

        $this->assertTrue($p->isOwner($u));
        
        $u2=factory(\App\User::class)->create();
        $this->assertFalse($p->isOwner($u2));

    }
}
