<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class NavTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */


    public function test_a_user_can_login()
    {
        $user = factory(\App\User::class)->create();
        $response=$this->actingAs($user)->get('/home')->assertSee("Hallo, ");
    }


    public function test_an_admin_can_create_a_product()
    {
        $user = factory(\App\User::class)->create();
        $admin = factory(\App\User::class)->create(["can_admin"=>true]);
        $response=$this->actingAs($user)->get('/products/create')->assertSee("Artikel erfassen");
    }
}
