<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProjectTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;
    public function test_projects_create_html()
    {
        $can_rent = factory(\App\User::class)->create();

        $response=$this->actingAs($can_rent)
                       ->get('/projects/create')
                       ->assertStatus(200);
    }
    public function test_projects_index()
    {
        $can_rent = factory(\App\User::class)->create(["can_rent"=>true]);
        $project = factory(\App\Project::class)->create();

        $response=$this->actingAs($can_rent)
                       ->get('/projects')
                       ->assertStatus(200);
    }

    public function test_projects_store_own_html()
    {
        $can_rent = factory(\App\User::class)->create(["can_rent"=>true]);

        $response=$this->actingAs($can_rent)
                       ->get('/projects/create')
                       ->assertStatus(200);
    }
}
