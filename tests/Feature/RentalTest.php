<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RentalTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */



    public function test_all_rentals_index() {
        $can_rent = factory(\App\User::class)->create(["can_rent"=>true]);
        $rental = factory(\App\Rental::class)->create(["status"=>"reserved"]);


        // update status to rented
        $response=$this->actingAs($can_rent)
                       ->withSession(["current_rental_id"=>$rental->id])
                       ->get(route("rentals.index"))
                       ->assertSee(__("Reserved"));


    }

    public function test_a_rental_can_be_updated()
    {
        $user = factory(\App\User::class)->create();
        $can_rent = factory(\App\User::class)->create(["can_rent"=>true]);
        $rental = factory(\App\Rental::class)->create(["status"=>"reserved"]);

        // see rental list
        $response=$this->actingAs($can_rent)
                       ->withSession(["current_rental_id"=>$rental->id])
                       ->get('/home')
                       ->assertSee("Aktuelle Leihliste");
        $this->assertEquals("reserved",$rental->status);


        // update status to rented
        $response=$this->actingAs($can_rent)
                       ->withSession(["current_rental_id"=>$rental->id])
                       ->put(route("rentals.update",$rental->id),["status"=>"rented"])
                       ->assertSee("Redirecting");

        $this->assertEquals("rented",$rental->fresh()->status);

    }
}
