function load_big_image(source) {
    document.getElementById('big_image').src = source;
}

function update_images() {
    var urls = document.getElementById("image_urls");
    const images = urls.value.split(',');
    const tiny_images = document.getElementById('tiny_images');
    /*
    var html = '';
    images.forEach((image) => {
        html+= '<img src='+image+' height=150>';
    });
    show_images_wrapper.innerHTML = html;
    */
    if (images.length != undefined || null || images != null || undefined) {
        var html_small = '';
        images.forEach((image) => {
            html_small += '<img class="" src="' + image + '" height="70" width="70" onclick="load_big_image(this.src)" style="cursor:pointer;">';
        });
        tiny_images.innerHTML = html_small;
        load_big_image(images[0]);
    } else {
        tiny_images.innerHTML = '<img src="https://ppc.tools/wp-content/themes/ppctools/img/no-thumbnail.jpg" height="70" width="70" style="cursor:pointer;" class="img-thumbnail">';
        load_big_image("https://ppc.tools/wp-content/themes/ppctools/img/no-thumbnail.jpg");
    }
}