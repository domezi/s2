<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();


Route::group(['middleware' => ['auth']], function() {

    Route::group(['middleware' => ['can_rent']], function() {
        Route::get('/users', 'UserController@index')->name('users.index');
        Route::get('/locations', 'LocationController@index')->name('locations.index');
        Route::get('/locations/create', 'LocationController@create')->name('locations.create');
        Route::post('/locations/create', 'LocationController@store')->name('locations.store');
        Route::get('/locations/{location}/edit', 'LocationController@edit')->name('locations.edit');
        Route::put('/locations/{location}/edit', 'LocationController@update')->name('locations.update');
        Route::get('/projects/{project}/edit', 'ProjectController@edit')->name('projects.edit');
        Route::put('/projects/{project}/edit', 'ProjectController@update')->name('projects.update');

        Route::get('/api/rental/current', 'SearchController@get_current_rental');
        Route::get('/api/qrcode/{qrcode}', 'SearchController@qrcode_results');
    });


    Route::group(['middleware' => ['can_admin']], function() {
        Route::post('/api/product/create-and-rent', 'ProductController@create_and_rent')->name('products.create_and_rent');

        Route::get('/products/create', 'ProductController@create')->name('products.create');
        Route::get('/products/{product}/edit', 'ProductController@edit')->name('products.edit');
        Route::put('/products/{product}/edit', 'ProductController@update')->name('products.update');
        Route::post('/products/create', 'ProductController@store')->name('products.store');
        Route::get('/users', 'UserController@index')->name('users.index');
        Route::get('/users/create', 'UserController@create')->name('users.create');
        Route::post('/users/create', 'UserController@store')->name('users.store');
        Route::put('/users/{user}', 'UserController@update')->name('users.update');
    });


    Route::get('/products', 'ProductController@index')->name('products.index');

    Route::get('/rentals', 'RentalController@index')->name('rentals.index');
    Route::post('/rentals', 'RentalController@store')->name('rentals.store');
    Route::put('/rentals/{rental}', 'RentalController@update')->name('rentals.update');
    Route::get('/rentals/{rental}/request_reservation', 'RentalController@request_reservation');
    Route::get('/rentals/{rental}', 'RentalController@show')->name('rentals.show');
    Route::get('/rentals/{rental}/pdf', 'RentalController@pdf')->name('rentals.pdf');

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/qrmode', 'SearchController@qrmode')->name('qrmode');

    Route::get('/search', 'SearchController@index')->name('search');
    Route::post('/products/rent/{product}', 'ProductController@rent')->name('products.rent');
    Route::get('/products/{product}', 'ProductController@show')->name('products.show');

    Route::post('/rentals/{rental}/product/{product}/pivot', 'ProductController@pivot')->name('rentals.products.pivot');

    Route::get('/projects/create', 'ProjectController@create')->name('projects.create');
    Route::post('/projects/create', 'ProjectController@store')->name('projects.store');
    Route::get('/projects', 'ProjectController@index')->name('projects.index');

    Route::post('/rentals/current_close', 'RentalController@current_close')->name('rentals.current_close');
    Route::post('/rentals/current_open/{rental}', 'RentalController@current_open')->name('rentals.current_open');

});
